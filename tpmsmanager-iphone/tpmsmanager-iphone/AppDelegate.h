//
//  AppDelegate.h
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 10/29/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCMain.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    VCMain *vcMain;
}

@property (strong, nonatomic) UIWindow *window;


@end

