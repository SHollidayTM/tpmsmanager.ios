//
//  VCAdvSearch.m
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 11/6/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCAdvSearch.h"

@interface VCAdvSearch ()

@end

@implementation VCAdvSearch

- (void)viewDidLoad{
    [super viewDidLoad];
    vcSensorSearch = [[VCSensorSearch alloc] initWithNibName:@"SensorSearch" bundle:nil];
    [vcSensorSearch.view setFrame:CGRectMake(0, 85, self.view.frame.size.width, self.view.frame.size.height-85)];
    [self.view addSubview:vcSensorSearch.view];
    vcServiceKitSearch = [[VCServiceKitSearch alloc] initWithNibName:@"ServiceKitSearch" bundle:nil];
    [vcServiceKitSearch.view setFrame:CGRectMake(0, 85, self.view.frame.size.width, self.view.frame.size.height-85)];
    [vcServiceKitSearch.view setHidden:YES];
    [self.view addSubview:vcServiceKitSearch.view];
    vcTroubleshooting = [[VCTroubleshooting alloc] initWithNibName:@"Troubleshooting" bundle:nil];
    [vcTroubleshooting.view setFrame:CGRectMake(0, 85, self.view.frame.size.width, self.view.frame.size.height-85)];
    [vcTroubleshooting.view setHidden:YES];
    [self.view addSubview:vcTroubleshooting.view];
}

- (IBAction)segChanged:(UISegmentedControl *)sender{
    switch([sender selectedSegmentIndex]){
        case 0:
            [vcSensorSearch.view setHidden:NO];
            [vcServiceKitSearch.view setHidden:YES];
            [vcTroubleshooting.view setHidden:YES];
            break;
        case 1:
            [vcSensorSearch.view  setHidden:YES];
            [vcServiceKitSearch.view setHidden:NO];
            [vcTroubleshooting.view setHidden:YES];
            break;
        case 2:
            [vcSensorSearch.view  setHidden:YES];
            [vcServiceKitSearch.view setHidden:YES];
            [vcTroubleshooting.view setHidden:NO];
            break;
    }
}

@end
