//
//  VCMain.m
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 10/29/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCMain.h"

@interface VCMain ()

@end

@implementation VCMain

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startLoading) name:@"StartLoading" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoading) name:@"StopLoading" object:nil];
    [[btnSettings layer] setShadowColor:[UIColor whiteColor].CGColor];
    [[btnSettings layer] setShadowOpacity:1];
    [[btnSettings layer] setShadowRadius:2];
    [[btnSettings layer] setShadowOffset:CGSizeMake(0, 0)];
    [barMain setSelectedItem:[[barMain items] objectAtIndex:0]];
    vcQuickSearch = [[VCQuickSearch alloc] initWithNibName:@"QuickSearch" bundle:nil];
    [vcQuickSearch.view setFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-99)];
    [self.view insertSubview:vcQuickSearch.view belowSubview:vLoading];
    vcAdvSearch = [[VCAdvSearch alloc] initWithNibName:@"AdvSearch" bundle:nil];
    [vcAdvSearch.view setFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-99)];
    [vcAdvSearch.view setHidden:YES];
    [self.view insertSubview:vcAdvSearch.view belowSubview:vLoading];
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)startLoading{
    [vLoading setHidden:NO];
}

- (void)stopLoading{
    [vLoading setHidden:YES];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    switch([item tag]){
        case 0:
            [vcQuickSearch.view setHidden:NO];
            [vcAdvSearch.view setHidden:YES];
            break;
        case 1:
            [vcQuickSearch.view setHidden:YES];
            [vcAdvSearch.view setHidden:NO];
            break;
        case 2:
            [vcQuickSearch.view setHidden:YES];
            [vcAdvSearch.view setHidden:YES];
            break;
        case 3:
            [vcQuickSearch.view setHidden:YES];
            [vcAdvSearch.view setHidden:YES];
            break;
        case 4:
            [vcQuickSearch.view setHidden:YES];
            [vcAdvSearch.view setHidden:YES];
            break;
    }
}

@end
