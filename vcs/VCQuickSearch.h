//
//  VCQuickSearch.h
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 10/29/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "DBModule.h"
#import "TokenModule.h"
#import "RetrievalModule.h"

@interface VCQuickSearch : UIViewController<AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>{
    IBOutlet UITextField *txtVIN;
    IBOutlet UIPickerView *pck;
    IBOutlet UIButton *btnSearch;
    IBOutlet UIButton *btnSpeed;
    IBOutlet UIView *vVidBlock;
    UIButton *btnCancelScan;
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *videoLayer;
    NSString *selectedYear;
    Make *selectedMake;
    Model *selectedModel;
    NSArray *years;
    NSArray *makes;
    NSArray *models;
    BOOL byVIN;
}

- (IBAction)scanVIN;
- (void)cancelScan;
- (IBAction)search;
- (IBAction)speed;

@end
