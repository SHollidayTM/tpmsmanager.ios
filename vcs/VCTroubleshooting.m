//
//  VCTroubleshooting.m
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 11/9/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCTroubleshooting.h"

@interface VCTroubleshooting ()

@end

@implementation VCTroubleshooting

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenResult:) name:@"TokenResultTroubleshooting" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retrievalResult:) name:@"RetrievalResultTroubleshooting" object:nil];
    [[btnSearch layer] setCornerRadius:10];
    [btnSearch setClipsToBounds:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int count = 0;
    switch(component){
        case 0:
            years = [[DBModule sharedInstance] getAllYears];
            count = (int)[years count];
            break;
        case 1:
            if(selectedYear > 0){
                makes = [[DBModule sharedInstance] getMakesByYear:selectedYear];
                count = (int)[makes count];
            }
            break;
        case 2:
            if(selectedMake){
                models = [[DBModule sharedInstance] getModelsByYearAndMake:selectedYear mke:selectedMake];
                count = (int)[models count];
            }
            break;
    }
    return count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSString *title;
    switch(component){
        case 0:
            if([[years objectAtIndex:row] isEqual:[NSNumber numberWithInt:0]]){
                title = @"";
            }else{
                title = [[years objectAtIndex:row] stringValue];
            }
            break;
        case 1:{
            Make *make = [makes objectAtIndex:row];
            title = [make name];
        }
            break;
        case 2:{
            Model *model = [models objectAtIndex:row];
            title = [model name];
        }
            break;
    }
    UILabel *lbl = [[UILabel alloc] init];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        [lbl setFont:[UIFont systemFontOfSize:30]];
    }else{
        [lbl setFont:[UIFont systemFontOfSize:14]];
    }
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5]];
    [lbl setText:title];
    [[[pickerView subviews] objectAtIndex:1] setHidden:YES];
    [[[pickerView subviews] objectAtIndex:2] setHidden:YES];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    switch(component){
        case 0:
            selectedYear = [years objectAtIndex:row];
            makes = [[DBModule sharedInstance] getMakesByYear:selectedYear];
            if([pickerView selectedRowInComponent:1] < [makes count]){
                selectedMake = [makes objectAtIndex:[pickerView selectedRowInComponent:1]];
            }else{
                selectedMake = [makes lastObject];
            }
            models = [[DBModule sharedInstance] getModelsByYearAndMake:selectedYear mke:selectedMake];
            if([pickerView selectedRowInComponent:2] < [models count]){
                selectedModel = [models objectAtIndex:[pickerView selectedRowInComponent:2]];
            }else{
                selectedModel = [models lastObject];
            }
            [pck reloadAllComponents];
            break;
        case 1:
            selectedMake = [makes objectAtIndex:row];
            [pck reloadAllComponents];
            break;
        case 2:
            selectedModel = [models objectAtIndex:row];
            break;
    }
}

- (IBAction)search{
    [self getToken];
}

- (void)getToken{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[TokenModule sharedInstance] getToken:3];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:nil];
        });
    });
}

- (void)tokenResult:(NSNotification *)notification{
    NSString *status = [[notification object] objectForKey:@"status"];
    if([status isEqualToString:@"200"]){
        [[RetrievalModule sharedInstance] getTPMS:NO req:3]; //not byVin, but by whatever else
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your token. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)retrievalResult:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *ctrAlert;
        NSString *status = [[notification object] objectForKey:@"status"];
        if([status isEqualToString:@"200"]){
            //display troubleshooting stuff
        }else{
            ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your troubleshooting information. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        }
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    });
}

@end
