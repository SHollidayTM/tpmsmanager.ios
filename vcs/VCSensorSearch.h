//
//  VCSensorSearch.h
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 11/6/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBModule.h"
#import "TokenModule.h"
#import "RetrievalModule.h"
#import "VCSingleSelector.h"

@interface VCSensorSearch : UIViewController{
    IBOutlet UITextField *txtPartNum;
    IBOutlet UITextField *txtFCCId;
    IBOutlet UITextField *txtManDist;
    IBOutlet UITextField *txtSensorType;
    IBOutlet UITextField *txtShape;
    IBOutlet UITextField *txtFrequency;
    IBOutlet UITextField *txtPartDesign;
    IBOutlet UITextField *txtManBy;
    IBOutlet UITextField *txtHousColor;
    IBOutlet UITextField *txtValveMat;
    IBOutlet UIButton *btnSearch;
    VCSingleSelector *vcSingleSelector;
}

- (IBAction)search;

@end
