//
//  VCTroubleshooting.h
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 11/9/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBModule.h"
#import "TokenModule.h"
#import "RetrievalModule.h"

@interface VCTroubleshooting : UIViewController{
    IBOutlet UIPickerView *pck;
    IBOutlet UIButton *btnSearch;
    NSString *selectedYear;
    Make *selectedMake;
    Model *selectedModel;
    NSArray *years;
    NSArray *makes;
    NSArray *models;
}

- (IBAction)search;

@end
