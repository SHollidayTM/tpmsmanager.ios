//
//  VCSensorSearch.m
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 11/6/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCSensorSearch.h"

@interface VCSensorSearch ()

@end

@implementation VCSensorSearch

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenResult:) name:@"TokenResultSensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retrievalResult:) name:@"RetrievalResultSensor" object:nil];
    [[btnSearch layer] setCornerRadius:10];
    [btnSearch setClipsToBounds:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.view endEditing:YES];
    if([textField isEqual:txtPartNum]){
        return YES;
    }else if([textField isEqual:txtFCCId]){
        return YES;
    }else if([textField isEqual:txtManDist]){
        vcSingleSelector = [[VCSingleSelector alloc] initWithNibName:@"SingleSelector" bundle:nil];
        [vcSingleSelector.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:vcSingleSelector.view];
    }else if([textField isEqual:txtSensorType]){
        
    }else if([textField isEqual:txtShape]){
        
    }else if([textField isEqual:txtFrequency]){
        
    }else if([textField isEqual:txtPartDesign]){
        
    }else if([textField isEqual:txtManBy]){
        
    }else if([textField isEqual:txtHousColor]){
        
    }else if([textField isEqual:txtValveMat]){
        
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (IBAction)search{
    [self getToken];
}

- (void)getToken{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[TokenModule sharedInstance] getToken:1];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:nil];
        });
    });
}

- (void)tokenResult:(NSNotification *)notification{
    NSString *status = [[notification object] objectForKey:@"status"];
    if([status isEqualToString:@"200"]){
        [[RetrievalModule sharedInstance] getTPMS:NO req:1]; //not byVin, but by whatever else
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your token. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)retrievalResult:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *ctrAlert;
        NSString *status = [[notification object] objectForKey:@"status"];
        if([status isEqualToString:@"200"]){
            //display sensor info
        }else{
            ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your sensor information. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        }
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    });
}

@end
