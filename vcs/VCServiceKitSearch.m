//
//  VCServiceKitSearch.m
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 11/9/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "VCServiceKitSearch.h"

@interface VCServiceKitSearch ()

@end

@implementation VCServiceKitSearch

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenResult:) name:@"TokenResultServiceKit" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retrievalResult:) name:@"RetrievalResultServiceKit" object:nil];
    [[btnSearch layer] setCornerRadius:10];
    [btnSearch setClipsToBounds:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [self.view endEditing:YES];
    if([textField isEqual:txtSensorPart]){
        return YES;
    }else if([textField isEqual:txtKitPart]){
        return YES;
    }else if([textField isEqual:txtManDist]){
        
    }
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [cell setBackgroundColor:[UIColor clearColor]];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, tableView.frame.size.width-4, 40)];
    [lbl setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.35]];
    switch(indexPath.row){
        case 0:
            [lbl setText:@" Cap"];
            break;
        case 1:
            [lbl setText:@" Core"];
            break;
        case 2:
            [lbl setText:@" Grommet"];
            break;
        case 3:
            [lbl setText:@" Metal Washer"];
            break;
        case 4:
            [lbl setText:@" Hex Nut"];
            break;
        case 5:
            [lbl setText:@" Valve"];
            break;
        case 6:
            [lbl setText:@" Complete Valve Assembly"];
            break;
        case 7:
            [lbl setText:@" Metal Valve Included"];
            break;
    }
    [cell.contentView addSubview:lbl];
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    return indexPath;
}

- (IBAction)search{
    [self getToken];
}

- (void)getToken{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[TokenModule sharedInstance] getToken:2];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:nil];
        });
    });
}

- (void)tokenResult:(NSNotification *)notification{
    NSString *status = [[notification object] objectForKey:@"status"];
    if([status isEqualToString:@"200"]){
        [[RetrievalModule sharedInstance] getTPMS:NO req:2]; //not byVin, but by whatever else
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your token. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)retrievalResult:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *ctrAlert;
        NSString *status = [[notification object] objectForKey:@"status"];
        if([status isEqualToString:@"200"]){
            //display service kit info
        }else{
            ctrAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"There was an error retrieving your service kit information. Please try again later." preferredStyle:UIAlertControllerStyleAlert];
        }
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    });
}

@end
