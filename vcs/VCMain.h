//
//  VCMain.h
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 10/29/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCQuickSearch.h"
#import "VCAdvSearch.h"

@interface VCMain : UIViewController{
    IBOutlet UIView *vLoading;
    IBOutlet UIButton *btnSettings;
    IBOutlet UITabBar *barMain;
    VCQuickSearch *vcQuickSearch;
    VCAdvSearch *vcAdvSearch;
}

@end
