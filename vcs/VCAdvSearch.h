//
//  VCAdvSearch.h
//  tpmsmanager-iphone
//
//  Created by Scott Holliday on 11/6/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCSensorSearch.h"
#import "VCServiceKitSearch.h"
#import "VCTroubleshooting.h"

@interface VCAdvSearch : UIViewController{
    IBOutlet UISegmentedControl *seg;
    VCSensorSearch *vcSensorSearch;
    VCServiceKitSearch *vcServiceKitSearch;
    VCTroubleshooting *vcTroubleshooting;
}

- (IBAction)segChanged:(UISegmentedControl *)sender;

@end
