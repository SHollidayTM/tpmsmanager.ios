//
//  RetrievalModule.m
//  quicktpms-ipad
//
//  Created by Scott Holliday on 11/4/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "RetrievalModule.h"

@implementation RetrievalModule

+ (id)sharedInstance{
    static RetrievalModule *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)getTPMS:(BOOL)byVIN req:(int)sentRequestor{
    requestor = sentRequestor;
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://id.tiremetrix.com/token"]];
    [request setHTTPMethod:@"POST"];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSString *authString;
    if(byVIN){
        NSString *vin = [[NSUserDefaults standardUserDefaults] objectForKey:@"vin"];
        authString = [NSString stringWithFormat:@"{\"vin\":\"%@\",\"token\":\"%@\"}", vin, token];
    }else{
        NSString *year = [[NSUserDefaults standardUserDefaults] objectForKey:@"year"];
        NSString *make = [[NSUserDefaults standardUserDefaults] objectForKey:@"make"];
        NSString *model = [[NSUserDefaults standardUserDefaults] objectForKey:@"model"];
        authString = [NSString stringWithFormat:@"{\"year\":\"%@\",\"make\":\"%@\",\"model\":\"%@\",\"token\":\"%@\"}", year, make, model, token];
    }
    [request addValue:@"application/json;" forHTTPHeaderField:@"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[authString length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:[authString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            dicStatus = @{@"status":@"500"};
            [self sendResult];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
        if(httpResp.statusCode != 200){
            dicStatus = @{@"status":[NSString stringWithFormat:@"%ld", (long)httpResp.statusCode]};
            [self sendResult];
        }else{
            //read data
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            dicStatus = @{@"status":@"200", @"vehicleData":dic};
            [self sendResult];
        }
    }];
    [task resume];
}

- (void)sendResult{
    switch(requestor){
        case 0:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResultQuick" object:dicStatus];
            break;
        case 1:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResultSensor" object:dicStatus];
            break;
        case 2:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResultServiceKit" object:dicStatus];
            break;
        case 3:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrievalResultTroubleshooting" object:dicStatus];
            break;
    }
}

@end
