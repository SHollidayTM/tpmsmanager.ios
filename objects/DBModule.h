//
//  DBModule.h
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Make.h"
#import "Model.h"
#import "Sensor.h"
#import "ServiceKit.h"

@interface DBModule : NSObject{
    sqlite3 *db;
}

@property (assign) BOOL inService;

+ (id)sharedInstance;
- (NSArray *)getAllYears;
- (NSArray *)getMakesByYear:(NSString *)year;
- (NSArray *)getModelsByYearAndMake:(NSString *)year mke:(Make *)make;
- (NSArray *)getSensorsByYearMakeAndModel:(NSString *)year mke:(int)makeID mod:(int)modelID;
- (NSArray *)getServiceKitsByYearMakeAndModel:(NSString *)year mke:(int)makeID mod:(int)modelID;

@end
