//
//  TokenModule.h
//  quicktpms-iphone
//
//  Created by Scott Holliday on 11/3/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>

@interface TokenModule : NSObject{
    int requestor;
    NSDictionary *dicStatus;
}

+ (id)sharedInstance;
- (void)getToken:(int)sentRequestor;

@end
